import { ClientFunction, Selector } from 'testcafe'
import axios from 'axios'

fixture `Test 3`
    .page `http://localhost:3001`
    .beforeEach( async browser => {
        try {
            const res = await axios.get('http://localhost:3000/devices')
            browser.ctx.res = res

            /*
            * Sort res.data by hdd_capacity because this
            * is how it is being sorted and rendered by the Frontend App
            */
            browser.ctx.data = res.data.sort((a, b) => {
                return a.hdd_capacity - b.hdd_capacity
            })
        } catch (error) {
            console.error(error)
        }
    })

test('Should make an API call that renames the first device of the list to “Renamed Device”', async browser => {
    try {
        const getPageUrl = ClientFunction(() => window.location.href)
        const firstDeviceEdit = Selector('.list-devices').child(0).child(1).child('.device-edit')
        const getFirstDeviceApiId = browser.ctx.data[0].id
        const systemNameInput = Selector('#system_name')

        await browser.click(firstDeviceEdit)
        await browser.expect(getPageUrl()).contains(`/devices/edit/${getFirstDeviceApiId}`)
        await browser.typeText(systemNameInput, 'Renamed Device', { replace: true })
        await browser.click('.submitButton')
        await browser.expect(getPageUrl()).eql('http://localhost:3001/')
    } catch (error) {
        console.error(error)
    }
})

test('Should Rrload the page and verify the modified device has the new name', async browser => {
    try {
        const firstDeviceName = Selector('.list-devices').child(0).child(0).child(0)

        await browser.expect(firstDeviceName.textContent).eql('Renamed Device')
    } catch (error) {
        console.error(error)
    }
})