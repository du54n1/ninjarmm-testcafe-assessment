import { Selector } from 'testcafe'
import axios from 'axios';

fixture `Test 1`
    .page `http://localhost:3001`
    .beforeEach( async browser => {
        try {
            const res = await axios.get('http://localhost:3000/devices')
            browser.ctx.res = res

            /*
            * Sort res.data by hdd_capacity because this
            * is how it is being sorted and rendered by the Frontend App
            */
            browser.ctx.data = res.data.sort((a, b) => {
                return a.hdd_capacity - b.hdd_capacity
            })
        } catch (error) {
            console.error(error)
        }
    })

test('Should make an API call to retrieve the list of devices', async browser => {
    try {
        // Assertions
        await browser.expect(browser.ctx.res.status).eql(200)
        await browser.expect(browser.ctx.res.statusText).eql('OK')
    } catch (error) {
        console.error(error)
    }
})

test('Should use the list of devices to check the elements are visible in the DOM', async browser => {
    try {
        await browser
        .expect(Selector('.device-main-box').count)
        .eql(browser.ctx.data.length)

        for (let i = 0; i < browser.ctx.data.length; i++) {
            // API Variables
            let deviceApiName = await browser.ctx.data[i].system_name
            let deviceApiType = await browser.ctx.data[i].type
            let deviceApiCapacity = await browser.ctx.data[i].hdd_capacity

            // UI Variables
            let deviceUiName = Selector('.list-devices').child(i).child(0).child(0)
            let deviceUiType = Selector('.list-devices').child(i).child(0).child(1)
            let deviceUiCapacity = Selector('.list-devices').child(i).child(0).child(2).find((node, index) => {
                return index === 0
            })

            // Assertions
            await browser.expect(deviceUiName.textContent).eql(deviceApiName)
            await browser.expect(deviceUiType.textContent).eql(deviceApiType)
            await browser.expect(deviceUiCapacity.textContent).eql(deviceApiCapacity)

        }
    } catch (error) {
        console.error(error)
    }
})

test('Should verify that all devices contain the EDIT and REMOVE buttons', async browser => {
    try {
        for (let i = 0; i < browser.ctx.data.length; i++) {
            // UI Variables
            let deviceUiEdit = Selector('.list-devices').child(i).child(1).child(0)
            let deviceUiDelete = Selector('.list-devices').child(i).child(1).child(1)

            // Assertions
            await browser.expect(deviceUiEdit.textContent).eql('Edit')
            await browser.expect(deviceUiDelete.textContent).eql('Remove')
        }
    } catch (error) {
        console.error(error)
    }
})