import { ClientFunction, Selector } from "testcafe"

fixture `Test 2`
    .page `http://localhost:3001`

test('Should verify that devices can be created properly using the UI', async browser => {
    try {
        const getPageUrl = ClientFunction(() => window.location.href)
        const systemNameInput = Selector('#system_name')
        const typeInput = Selector('#type')
        const capacityInput = Selector('#hdd_capacity')

        // Asserts all elements to add a new device are present
        await browser.click('.submitButton')
        await browser.expect(getPageUrl()).contains('/devices/add')
        await browser.expect(Selector('.device-form').exists).ok()
        await browser.expect(Selector('[name="system_name"]').exists).ok()
        await browser.expect(Selector('#type').child().count).eql(3)
        await browser.expect(Selector('[name="hdd_capacity"]').exists).ok()
        await browser.expect(Selector('.submitButton').exists).ok()

        // Add a new device
        await browser.typeText(systemNameInput, 'System Test Name')
        await browser.click(typeInput)
        await browser.click(typeInput.child(1))
        await browser.typeText(capacityInput, '8')
        await browser.click('.submitButton')
        await browser.expect(getPageUrl()).eql('http://localhost:3001/')
    } catch (error) {
        console.error(error)
    }
})

test('Should verify the new device is now visible', async browser => {
    try {
        // New device selectors
        const newDeviceName = Selector('.list-devices').child(0).child(0).child(0)
        const newDeviceType = Selector('.list-devices').child(0).child(0).child(1)
        const newDeviceCapacity = Selector('.list-devices').child(0).child(0).child(2).find((node, index) => {
            return index === 0
        })

        // Assertions
        await browser.expect(newDeviceName.textContent).eql('System Test Name')
        await browser.expect(newDeviceType.textContent).eql('WINDOWS_SERVER')
        await browser.expect(newDeviceCapacity.textContent).eql('8')
    } catch (error) {
        console.error(error)
    }
})