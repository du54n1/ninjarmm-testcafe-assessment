import { ClientFunction, Selector } from 'testcafe'
import axios from 'axios'

fixture `Test 4`
    .page `http://localhost:3001`
    .beforeEach( async browser => {
        try {
            const res = await axios.get('http://localhost:3000/devices')
            browser.ctx.res = res

            /*
            * Sort res.data by hdd_capacity because this
            * is how it is being sorted and rendered by the Frontend App
            */
            browser.ctx.data = res.data.sort((a, b) => {
                return a.hdd_capacity - b.hdd_capacity
            })
        } catch (error) {
            console.error(error)
        }
    })

test('Should make an API call that deletes the last element of the list', async browser => {

    const lastDeviceRemove = Selector('.list-devices').child(browser.ctx.data.length - 1).child(1).child('.device-remove')

    await browser.click(lastDeviceRemove)
})

test('Should reload the page and verify the element is no longer visible and it doesn\’t exist in the DOM', async browser => {
    await browser.expect(Selector('.device-main-box').count).eql(browser.ctx.data.length)
})